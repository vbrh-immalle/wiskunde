+++
date = 2015-10-01T18:00:00Z
title = "Excel online"
+++

Op sommige oef. (zoals [Oef EI]({{< relref "finalg/01_OefEI.md" >}}) en
[Oef SI]({{< relref "finalg/02_OefSI.md" >}}))
staan ingesloten koppelingen naar [Excel Online](https://office.live.com/start/Excel.aspx) spreadsheets dewelke  opgeslagen zijn op een *OneDrive*-account.

De spreadsheets kunnen:

- gedownload worden
- live aangepast worden om te experimenteren (het origineel op *OneDrive* zal niet wijzigen)

Als iemand met de juiste rechten (de adminstrator van deze site) het origineel op *OneDrive* wijzigt, zal (na een refresh v.d. pagina) de nieuwste data beschikbaar zijn.

> **Tip:** Bekijk de formules in de spreadsheets!

+++
date = 2015-10-01T19:00:00Z
title = "Layout van deze site"
+++

De layout van deze site bevat nog wat foutjes.

De broncode is nog steeds beschikbaar op [Gitlab](https://gitlab.com/hansvb-immalle/wiskunde).
Leerlingen die de CSS-code kunnen verbeteren of die een volledig alternatieve
layout hebben gemaakt, kunnen een *Merge Request* indienen! `;-)`

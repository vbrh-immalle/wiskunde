+++
date = 2015-09-30T00:00:00Z
title = "Welkom"
+++

6IB kan deze pagina gebruiken als naslagwerk of ter controle van de eigen
notities wiskunde.

Deze pagina wordt met [Hugo](https://gohugo.io/) onderhouden.

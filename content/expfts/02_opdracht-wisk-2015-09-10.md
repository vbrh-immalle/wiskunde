+++
title = "Opdracht 2015-09-10 : groeifactoren en percentages"
math = true
+++

# Groeifactor

Omdat een getal vermenigvuldigen met 1 terug datzelfde getal oplevert,
kunnen we procentuele stijgen en dalingen gemakkelijk omzetten naar een **factor**:

$ groeifactor = 1 + percentage_{decimaal} = 1 + \frac{percentage}{100} $

Merk op dat percentage ook negatief kan zijn, b.v. *een korting van 20%*.


## stijging van 5%

De groeifactor wordt : $ 1,05 = 1 + 0,05 = 1 + \frac{5}{100} $

Een getal dat 5% stijgt, is te beschouwen als:

- een vermenigvuldiging van dat getal met 1,05
- een vermenigvuldiging van dat getal met 1 **vermeerderd** met een vermenigvuldiging met 0,05 van dat getal

$x(1+0,05) = 1,05x$

b.v. $ 1 \cdot 1000 + 0,05 \cdot 1000 = 1,05 \cdot 1000 $

## daling van 5%

De groeifactor wordt : $ 0,95 = 1 - 0,05 = 1 - \frac{5}{100} $

Een getal dat 5% daalt, kunnen we zien als *het overblijven* van 95% van dat getal.
Of ook:

- een vermenigvuldiging van dat getal met 0,95
- een vermenigvuldiging van dat getal met 1 **verminderd** met een vermenigvuldiging van 0,05 van dat getal

$x(1-0,05) = 0,95x$

b.v. $ 1 \cdot 1000 - 0,05 \cdot 1000 = 0,95 \cdot 1000 $

## verdubbeling

Een verdubbeling van x kunnen we beschouwen als een **stijging van 100%**:

$2x = x(1+1)$

> de tweede 1 stelt hier 100% voor

200% v.e. getal is dus een een **stijging** van 100% van dat getal.

## naamgeving

> "factor" is iets dat je vermenigvuldigt

> "groeifactor" is een factor die aanduidt hoe sterk iets groeit

> "negatieve groeifactor" is een factor die een verkleining aanduidt:
  een getal tussen 0 en 1

## denkvraagjes

Wat gebeurt er als de groeifactor

- $ g = 0 $ ?
- $ g = 1 $ ?
- $ g = 2 $ ?
- $ g = 0,5 $ ?



## Oefeningen percentages

> Los deze oef. op met voldoende tussenstappen.

> Gebruik in de formules de decimale voorstelling v.d. percentages om het rekenwerk aanschouwlijker te maken.

> Geef ook telkens de groeifactor (gebruik symbool $ u $).

Er zijn 3 soorten problemen die je kan tegenkomen:

1. Vorige maand betaalde je voor 13 euro GSM-kosten. Deze maand betaal je 5% meer. Hoeveel betaal je deze maand?
2. Deze maand betaal je 20 euro GSM-kosten, vorige maand slechts 15 euro. Hoeveel percent heb je meer betaald?
3. Je betaalt deze maand 30 euro GSM-kosten. Dat is 25% meer dan vorige maand. Hoeveel betaalde je vorige maand?

En uiteraard gaat dit ook voor prijsdalingen:

1. Er is 20% korting op een kledingstuk van 200 euro. Hoeveel moet je nog betalen?
2. Een kledingstuk dat oorspronkelijk 200 euro kostte, kost nu nog 120 euro. Hoeveel percent korting heb je gekregen?
3. Je hebt 200 euro betaald voor een kledingstuk waarop 20% korting was. Wat was de originele prijs?

Oplossingswijze:

1. $ eindbedrag = beginbedrag \cdot percentage_{decimaal} $
2. $ percentage_{decimaal} = \frac{eindbedrag}{beginbedrag} $
3. $ beginbedrag = \frac{eindbedrag}{percentage_{decimaal}} $

**Opgelet:** Bij kortingen is er een daling v.h. bedrag.
Een korting van 20% kan je beschouwen als *het overblijven van 80% v.d. oorspronkelijke prijs*.


# Logaritmische functies

Logaritmische functies zijn het inverse van exponentiële functies.

Ze hebben, net zoals exponentiële functies, steeds een **grondtal**.

B.v. de exponentiële functie $ f(x) = 10^x $ heeft 10 als grondtal.

De logaritmische functie $ f(x) = \log_{10} x $ heeft eveneens 10 als grondtal.

Voorbeelden:

- $ \log _{10} 1000 = \log _{10} 10^3 = 3 $
- $ \log _{10} 1000000 = \log _{10} 10^6 = 6 $
- $ \log _{2} 8 = \log _{2} 2^3 = 3 $
- $ \log _{2} 256 = \log _{2} 2^8 = 8 $

We kunnen dus algemeen zeggen:

$ \log_{a} x = y \Leftrightarrow a^y = x $

Merk op dat een logaritme ook gerust een reëel getal als grondtal kunnen hebben.
Los b.v. volgende logaritmen op (met je rekenmachine):

1. $ \log_{1,02} 1000 $
2. $ \log_{0,85} 1000 $
3. $ \log_{1,50} 1000 $
4. $ \log_{\frac{1}{2}} 1000 $

Enkele praktische vraagstukken die we kunnen oplossen met logaritmische functies:

- Na hoeveel delingen is 1 cel 1000 cellen geworden? (antwoord: $ log _{2} 1000 $ + afronden)
- Hoeveel cijfers heb je (in het decimaal talstelsel) nodig om
100 verschillende getallen voor te kunnen stellen?
(antwoord: $ log _{10} 100)
- Hoeveel bits heb je nodig om 200 verschillende getallen voor te stellen
(uiteraard in het binair talstelsel)?
(antwoord: $ log _{2} 200 $ + afronden)

We kunnen van het verband tussen exponentiële en logaritmische functie
gebruik maken om vergelijkingen op te lossen waarvan de veranderlijke in de macht staat.

b.v.

geg: $ z = a^y + 5 $

gevr: $ y $

opl: $ z = a^y + 5 $

$\Leftrightarrow z - 5 = a^y $

$ \Leftrightarrow \log _{a} (z-5) = \log _{a} a^y = y $

of

$ y = \log _{a} (z-5) $


# Herhaling machten

Herinner je volgende rekenregels i.v.m. machten:

- $ a^x \cdot a^y = a^{x+y} $ (b.v. $ 2^3 \cdot 2^4 = 2^7 $)
- $ a^{-x} = \frac{1}{a^x} $ (b.v. $ 10^{-3} = \frac{1}{10^3} = \frac{1}{1000} = 0,001 $)
- $ (a^x)^y = a^{x \cdot y} $ (b.v. $ (2^3)^4 = 2^{3 \cdot 4} = 2^{12} $)
- $ \sqrt{a} = a^{\frac{1}{2}} $
- $ \sqrt[3]{a} = a^{ \frac{1}{3} } $

# Oefening: financiële algebra

Herinner je de functie van samengestelde intrest:

$ K = k \cdot u^n $ of $ K = k \cdot (1 + i)^n $

Hiermee kan je een vraagstuk oplossen waarvoor $K$ gevraagd is
en $k$, $i$ en $n$ gegeven zijn.
(B.v. Ik heb een beginkapitaal van 1000 euro en zet dit 10 jaar
op een spaarrekening met een intrest van 2%. Hoeveel staat er na 10
jaar op mijn rekening?)

Herwerk deze functie zodanig dat vraagstukken kunnen opgelost worden met:

- $k$ gevraagd en $K$, $i$, $n$ gegeven
- $n$ gevraagd en $k$, $K$, $i$ gegeven
- $i$ gevraagd en $k$, $K$, $n$ gegeven

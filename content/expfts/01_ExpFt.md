+++
title = "Herhaling exponentiële functies"
math = "true"
+++

# Exponentiële functies

Algemene formule:

$f(x) = b \cdot g^x$

waarbij:

- $b$ : beginwaarde
- $g$ : groeifactor
- $x$ : variable
- $f(x)$ : de functie (in functie van de variabele $ x $)

Uiteraard kunnen we ook andere symbolen gebruiken, b.v.

$f(x) = b \cdot g^n$

waarbij $n$ dan b.v. een natuurlijk getal ($\in \mathbb{N}$) is i.p.v. een reëel getal ($\in \mathbb{R}$).

In de financiële algebra schrijft men de berekening van **samengestelde intrest**
(zoals die van een termijnrekening zoals een zicht- of spaarrekening) soms als

$K = k \cdot u^n$

waarbij:

- $K$ : eindkapitaal
- $k$ : beginkapitaal
- $u$ : rentefactor = 1 + i (waarbij i de rentevoet of intrest voorstelt)
- $n$ : looptijd, b.v. aantal jaar


Enkele voorbeelden van **exponentiële functies** :

$f(x) = 2^x$

> $f(x)$ heeft een groeifactor $2$. $f(x)$ verdubbelt dus steeds voor elke volgende (gehele) waarde van $x$.

$f(x) = 1,15^x$

> $f(x)$ heeft een groeifactor van 1,15.

> Voor elke volgende (gehele) waarde van $x$, vermenigvuldigen we dus met 1,15.

> Er komt m.a.w. telkens 15 % bij de vorige waarde bij.

$f(x) = 0,25^x$

> $f(x)$ heeft een groeifactor van 0,25. (**Opgepast:** dit is een daling!)

> Voor elke volgende (gehele) waarde van $x$, vermenigvuldigen we met 0,25 of $\frac{1}{4}$.

> Er wordt dus van $f(x)$ telkens 75% afgetrokken (want we houden telkens maar 25% over)
> (en 1 - 0,75 = 0,25 en 1 = 100%)

$K = k \cdot u^n = 2000 \cdot 1,034^5$

> Hiermee kan het eindkapitaal berekend worden voor een beginkapitaal van 2000 euro
met een rentevoet van 3,4% gedurende 5 jaar.


Onthoud:

> Bij een exponentiële functie, staat de veranderlijke ($x$, $n$, ...) in de **exponent** !

Ter vergelijking enkele **lineaire** functies :

$f(x) = 3,2 \cdot x$

> *$f(x)$ stijgt lineair met een factor 3,2*

$f(x) = 4,2x + 5$

> *$f(x)$ stijgt lineair met een factor 4,2 en is 5 als $x = 0$*

$f(x) = -20x$

> *$f(x)$ daalt lineair met een factor 20*

$I = k \cdot i$

> De intrest die een beginkapitaal $k$ gedurende 1 jaar opbrengt aan een rentevoet $i$ bedraag $I$ euro.
B.v. $2000 \cdot 0,05 = 100$


## Oefeningen

- Stel voor elk van voorgaande (of van soortgelijke) voorbeelden op papier een tabel op met 2 rijen: $x$ en $f(x)$
- Gebruik je rekenmachine om van enkele lineaire en exponentiële functies een tabel te genereren
- (software-pakketten) Maak in Excel of Google Spreadsheet een rekenblad waarin je een tabel maakt met verschillende getallenreeksen:
	- op rij 1 zet je de getallen -10 t.e.m. 10
	- op rij 2 zet je een lineaire formule, b.v. `=A1*3` (dit komt overeen met $f(x)=3 \cdot x$)
	- op rij 3 zet je een exponentiële formule, b.v. `=3^A1` ($f(x)=3^x$) of `=0.5^A1` ($f(x)=0,5^x)
	- enz ...
	- (uiteraard gebruik je de **vulgreep** om de formules te kopiëren)
	- Beeld naast elkaar de grafieken af van deze getallenreeksen.
	- Onderzoek wat er gebeurt met bij exponentiële functies met groeifactoren > 1 en groeifactoren tussen 0 en 1
- (software-ontwikkeling) Schrijf 2 functies in javascript met 2 parameters `b` en `g` die $f(x)$ v.d.
  exponentiële functie returnen:
	- `function expft1(b,g)` maakt enkel gebruik van `+` en `-` en een for-lus
	- `function expft2(b,g)` gebruikt `*` en de gepaste `Math`-functie



# Decimale voorstelling van percenten

Per cent = Pour cent = per 100

(Pro mille = per 1000)

$ 15\% = \frac{15}{100} = 0,15 $

$ 15&#x2030; = \frac{15}{1000} = 0,015 $

$ 3,4\% = \frac{3,4}{100} = 0,034 $


# Groeifactor

Omdat een getal vermenigvuldigen met 1 terug datzelfde getal oplevert,
kunnen we procentuele stijgen en dalingen gemakkelijk omzetten naar een **factor**:

$ groeifactor = 1 + percentage_{decimaal} = 1 + \frac{percentage}{100} $

Merk op dat percentage ook negatief kan zijn, b.v. *een korting van 20%*.


## stijging van 5%

De groeifactor wordt : $ 1,05 = 1 + 0,05 = 1 + \frac{5}{100} $

Een getal dat 5% stijgt, is te beschouwen als:

- een vermenigvuldiging van dat getal met 1,05
- een vermenigvuldiging van dat getal met 1 **vermeerderd** met een vermenigvuldiging met 0,05 van dat getal

$x(1+0,05) = 1,05x$

b.v. $ 1 \cdot 1000 + 0,05 \cdot 1000 = 1,05 \cdot 1000 $

## daling van 5%

De groeifactor wordt : $ 0,95 = 1 - 0,05 = 1 - \frac{5}{100} $

Een getal dat 5% daalt, kunnen we zien als *het overblijven* van 95% van dat getal.
Of ook:

- een vermenigvuldiging van dat getal met 0,95
- een vermenigvuldiging van dat getal met 1 **verminderd** met een vermenigvuldiging van 0,05 van dat getal

$x(1-0,05) = 0,95x$

b.v. $ 1 \cdot 1000 - 0,05 \cdot 1000 = 0,95 \cdot 1000 $

## verdubbeling

Een verdubbeling van x kunnen we beschouwen als een **stijging van 100%**:

$2x = x(1+1)$

> de tweede 1 stelt hier 100% voor

200% v.e. getal is dus een een **stijging** van 100% van dat getal.

## naamgeving

> "factor" is iets dat je vermenigvuldigt

> "groeifactor" is een factor die aanduidt hoe sterk iets groeit

> "negatieve groeifactor" is een factor die een verkleining aanduidt:
  een getal tussen 0 en 1

## vraagjes

Wat gebeurt er als de groeifactor

- $ g = 0 $ ?
- $ g = 1 $ ?
- $ g = 2 $ ?
- $ g = 0,5 $ ?



# Oefeningen percentages

> Los deze oef. op met voldoende tussenstappen.

> Gebruik in de formules de decimale voorstelling v.d. percentages om het rekenwerk aanschouwlijker te maken.

> Geef ook telkens de groeifactor (gebruik symbool $ u $).

Er zijn 3 soorten problemen die je kan tegenkomen:

1. Vorige maand betaalde je voor 13 euro GSM-kosten. Deze maand betaal je 5% meer. Hoeveel betaal je deze maand?
2. Deze maand betaal je 20 euro GSM-kosten, vorige maand slechts 15 euro. Hoeveel percent heb je meer betaald?
3. Je betaalt deze maand 30 euro GSM-kosten. Dat is 25% meer dan vorige maand. Hoeveel betaalde je vorige maand?

En uiteraard gaat dit ook voor prijsdalingen:

1. Er is 20% korting op een kledingstuk van 200 euro. Hoeveel moet je nog betalen?
2. Een kledingstuk dat oorspronkelijk 200 euro kostte, kost nu nog 120 euro. Hoeveel percent korting heb je gekregen?
3. Je hebt 200 euro betaald voor een kledingstuk waarop 20% korting was. Wat was de originele prijs?

Oplossingwijze:

1. $ eindbedrag = beginbedrag \cdot percentage_{decimaal} $
2. $ percentage_{decimaal} = \frac{eindbedrag}{beginbedrag} $
3. $ beginbedrag = \frac{eindbedrag}{percentage_{decimaal}} $

**Opgelet:** Bij kortingen is er een daling v.h. bedrag.
Een korting van 20% kan je beschouwen als *het overblijven van 80% v.d. oorspronkelijke prijs*.

# Intuïtief: percentages $ < 100 \% $

Intuïtief zou het ook duidelijk moeten zijn dat een percentage $ < 100 \% $ altijd een getal is tussen 0 en 1:

- Als je hiermee een getal vermenigvuldigt, verkleint het resultaat
- Als je hiermee een getal deelt, vergroot het resultaat

Als je 2 getallen door elkaar deelt, zie je hun onderlinge verhouding.
Dit is een breuk die decimaal kan worden weergegeven
maar ook als een percentage.

# Intuïtief: percentages $ > 100 \% $

Er bestaan natuurlijk percentages groter dan 100%, vbdn. :

- 200% = $\frac{200}{100} = 2 $ = een verdubbeling
- 500% = $\frac{500}{100} = 5 $ = een vervijfvoudiging
- 150% = $\frac{150}{100} = 1,5 $ = vermenigvuldigen met 1,5
- 100% = $\frac{100}{100} = 1 $ = vermenigvuldigen met 1
- ...

Opgelet:

- 200% v.e. getal is een verdubbeling
- 200% **stijging** v.e. getal is een verdriedubbeling (1 + 2 = 3)
- 100% stijging v.e. getal is een verdubbeling (1 + 1 = 2)

# Taalkundig

In ons tiendelig (decimaal) talstelsel hebben **cijfers** een andere waarde naargelang hun plaats in een **getal**.

In het getal 1234,567 zijn b.v. :

- **1** de duizendtallen
- **2** de hondertallen
- **3** de tientallen
- **4** de eenheden
- **5** de tienden
- **6** de honderdsten
- **7** de duizendsten

0,5% is dus 0,5 honderdsten, 5 duizendsten of zelfs 50 tienduizendsten.

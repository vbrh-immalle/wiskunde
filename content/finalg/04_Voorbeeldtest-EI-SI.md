+++
date = 2015-10-01T10:00:00Z
title = "Voorbeeldtest Enkelvoudige en Samengestelde Intrest"
math = true
tags = [ "oef" ]
+++

# Opgave

1. (E.I.) Ik kasbons kopen die jaarlijks 18% intrest uitbetalen.
Ik wil elke maand een coupon van minstens 500 euro intrest kunnen afnemen.
Hoe groot moet het kapitaal `k` zijn?

2. (S.I.) Een spaarboekje levert 2% intrest per maand.
Hoeveel jaar is er nodig om mijn kapitaal te verdubbelen?

3. (S.I.) Tegen welke intrest moet ik kunnen beleggen als ik nu 1000 euro heb en
binnen 10 jaar 3000 euro wil hebben?

# Oplossing

## 1

We berekenen eerst de maandelijke intrestvoet:

$$
\begin{align}
i_{12}  &= \frac{i}{12}     \cr
        &= \frac{0,18}{12}  \cr
        &= 0,015            \cr
        &= 1,5 \%
\end{align}
$$

We vertrekken van de formule voor enkelvoudige intrest:

$$
\begin{align}
                && I &= k \cdot i \cdot n          \cr
\Leftrightarrow && k &= \frac{I}{i \cdot n}        \cr
                &&   &= \frac{500}{0,015 \cdot 1}  \cr
                &&   &= 33333,33
\end{align}
$$

$$  $$


## 2

Verdubbelen wil zeggen dat

$$
\begin{align}
                &&           K &= 2 \cdot k   \cr
\Leftrightarrow && \frac {K}{k} &= 2
\end{align}
$$

Een intrest van 2% per maand wil zeggen
een groeifactor per maand:

$$
\begin{align}
u _{12} &= 1 + 0,02   \cr
        &= 1,02
\end{align}
$$

We vertrekken v.d. formule van gelijkwaardige rentevoeten:

$$
\begin{align}
                &&  u &= u _{12}^{12}   \cr
\Leftrightarrow &&  u &= u _{12}^{12}   &(1)\cr
                &&    &= 1,02^{12}      \cr
                &&    &= 1,2682         &(2)
\end{align}
$$

1. We zoeken de jaarlijke groeifactor $u$ of $u_1$ dus moeten de formule niet herwerken.
2. Voor de exactheid v.d. eindoplossing, werken we liever zo lang mogelijk met de exacte waarde $1,02^{12}$ i.p.v. met de afronding $1,2682$.

Nu gebruiken we de basisformule van samengestelde intrest:

$$
\begin{align}
                &&           K &= k \cdot u^n          \cr
\Leftrightarrow && \log _u u^n &= \log _u \frac{K}{k}  \cr
\Leftrightarrow &&          n  &= \log _u \frac{K}{k}   \cr
                &&             &= \log _u 2
\end{align}
$$

Merk nogmaals op dat we het eindresultaat beter bepalen met de exacte waarde dan met een afgeronde waarde:

$$
\begin{align}
n &= \log _{1,02^{12}} 2   \cr
  &= \log _{1,2682} 2      \cr
  &= 2,92
\end{align}
$$

## 3

We vertrekken v.d. formule van eindkapitaal bij samengestelde intrest.

$$
\begin{align}
                &&                               K &= k \cdot u^n                   \cr
\Leftrightarrow &&                               K &= k \cdot (1+i)^n               &(1)\cr
\Leftrightarrow &&                     \frac{K}{k} &= (1+i)^n                       &(2)\cr
\Leftrightarrow &&     (\frac{K}{k})^{\frac{1}{n}} &= (1+i)^{n \cdot \frac{1}{n}}   &(3)\cr
\Leftrightarrow &&     (\frac{K}{k})^{\frac{1}{n}} &= (1+i)                         &(4)\cr
\Leftrightarrow && (\frac{K}{k})^{\frac{1}{n}} - 1 &= i
\end{align}
$$

1. $i$ is gevraagd dus moeten we $u$ schrijven als $(1 + i)$
2. De $n$-de-macht moet weggewerkt worden, dus moeten we deze afzonderen
3. De $n$-de-machtswortel nemen van elk lid
4. Macht vereenvoudigen


Invullen:

$$
\begin{align}
i &= (\frac{K}{k})^{\frac{1}{n}} - 1          \cr
  &= (\frac{3000}{1000})^{\frac{1}{10}} - 1   \cr
  &= ...
\end{align}
$$

+++
date = 2015-09-28T00:00:00Z
title = "Gelijkwaardige rentevoeten"
math = true
+++

# Tijdsmetingen

In 1 jaar zitten:

- 2 semesters
- 4 trimesters of kwartalen
- 12 maanden
- 365 dagen

# Eindkapitalen

Bepaal het eindkapitaal $ K $ in alle gevallen:

1. $ i_{12} = 0,01 = 1 $ % intrest per maand
3. $ i_{2} = 6 $ % intrest per semester
2. $ i_{1} = 0,12 = 12 $ % intrest per jaar

Oplossing:

1. $ K = k \cdot u_{12}^{12} = k \cdot 1,01^{12} = k \cdot 1,1268... $
2. $ K = k \cdot u_{2}^2 = k \cdot 1,06^2 = k \cdot 1,1236 $
3. $ K = k \cdot u_{1} = k \cdot 1,12 = k \cdot 1,12 $

Je moet bij samengestelde intrest dus weer rekening houden met het cumulerende
effect v.d. intrest!

Met welke jaarlijkse rentevoet
$i _{1}$ komt $i _{12}$ dan wel overeen?

$ K / k = u _{12}^{12} = u _{4}^{4} = u _{2}^{2} = u $

Algemeen:

$ u_p^p = u_q^q $

$\Leftrightarrow (1+i)_p^p = (1+i)_q^q $

- semestrieel: $ u_2 $
- trimestrieel: $ u_4 $
- maandelijks: $ u_{12} $
- dagelijks: $ u_{365} $

# Voorbeeld

Een jaarlijkse intrest van 2% komt overeen met een maandelijke intrest van:

$$
\begin{align}
u _1 = 1,02
\end{align}
$$

en

$$
\begin{align}
                && u _{12}^{12} &= u_1                  \cr
\Leftrightarrow &&      u _{12} &= \sqrt[12]{u_1}       \cr
                &&      u _{12} &= u _1^{\frac{1}{12}}  \cr
                &&      u _{12} &= 1,02^{\frac{1}{12}}  \cr
                &&              &= 1,001652
\end{align}
$$

Bijgevolg komt een jaarlijkse intrest van 2% overeen met een maandelijke intrest van

$$
\begin{align}
                && u _{12} &= i _{12} + 1      \cr
\Leftrightarrow && i _{12} &= u _{12} - 1      \cr
                &&         &= 0,001652        \cr
                &&         &= 0,1652 \%
\end{align}
$$

Zoals te verwachten is dit minder dan de **nominale** maandelijke intrest:

$$
\begin{align}
  i _{12nominaal}  &= \frac {0,02} 2    \cr
                   &= 0,001667        \cr
                   &= 0,1667 \%
\end{align}
$$

Bij de reële maandelijkse intrest, speelt immers het cumulatieve effect van samengestelde intrest mee.

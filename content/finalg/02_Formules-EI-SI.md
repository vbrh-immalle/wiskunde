+++
date = 2015-09-18T00:00:00Z
title = "Formules EI en SI"
math = true
+++

# Enkelvoudige Intrest (E.I.)

Basisformule:

$$
\begin{align}
K &= k + I \cr
  &= k + k \cdot i \cdot n
\end{align}
$$

waarbij:

- $k$ : startkapitaal
- $i$ : intrestvoet (in decimale procenten)
- $n$ : aantal perioden (meestal jaar)
- $K$ : eindkapitaal
- $I$ : totale intrest $=k \cdot i \cdot n$

# Samengestelde Intrest (S.I.)

Basisformule:

$$
\begin{align}
K &= k \cdot u^n \cr
  &= k \cdot (1+i)^n
\end{align}
$$

waarbij:

- $K$ : eindkapitaal
- $k$ : beginkapitaal
- $u$ : rentefactor = $1 + i$ (waarbij i de decimale rentevoet of intrestvoet voorstelt) = $1 + \frac{p}{100}$ waarbij p de rentevoet in percent voorstelt)
- $n$ : looptijd, b.v. aantal jaar

# Afleidingen basisformule E.I.

Enkele voorbeelden:

1. geg: $k$, $i$, $n$ - gevr: $I$, $K$
2. geg: $i$, $n$, $K$ - gevr: $k$, $I$
3. geg: $k$, $n$, $K$ - gevr: $i$, $I$
4. geg: $k$, $i$, $I$ - gevr: $n$, $K$
5. geg: $I$, $i$, $n$ - gevr: $k$
6. ...

## geg: $k$, $i$, $n$ - gevr: $I$, $K$

$$
\begin{align}
I &= k \cdot i \cdot n \cr
K &= k + k \cdot i \cdot n
\end{align}
$$

## geg: $i$, $n$, $K$ - gevr: $k$, $I$

$$
\begin{align}
                &&                        K &= k + k \cdot i \cdot n    &(1)\cr
\Leftrightarrow &&                        K &= k \cdot (1 + i \cdot n)  &(2)\cr
\Leftrightarrow &&  \frac{K}{1 + i \cdot n} &= k &(3)\cr
\Leftrightarrow &&                        k &= \frac{K}{1 + i \cdot n}  &(4)
\end{align}
$$

1. basisformule
2. gemeenschappelijke factor $k$ buitenzetten
3. beide leden delen door $(1+i \cdot n)$
4. leden van plaats verwisselen

$$
\begin{align}
I &= k \cdot i \cdot n &(5)   \cr
  &= \left( \frac{K \cdot i \cdot n}{1 + i \cdot n} \right) &(6)
\end{align}
$$

$(6)$ is enkel nuttig indien rechtstreeks $I$ moet berekend worden,
b.v. in een Excel-formule.
Anders kan gewoon het berekende getal $k$ (uit $(4)$) ingevuld worden in $(5)$.

## geg: $k$, $n$, $K$ - gevr: $i$, $I$

$$
\begin{align}
                &&                          K &= k + k \cdot i \cdot n   &(1)\cr
\Leftrightarrow &&                          K &= k \cdot (1 + i \cdot n) &(2)\cr
\Leftrightarrow &&                \frac{K}{k} &= 1 + i \cdot n           &(3)\cr
\Leftrightarrow &&            \frac{K}{k} - 1 &= i \cdot n               &(4)\cr
\Leftrightarrow &&  \frac{\frac{K}{k} - 1}{n} &= i                       &(5)
\end{align}
$$

1. basisformule
2. gemeenschappelijke factor buitenzetten
3. beide leden delen door $k$
4. $-1$ van lid veranderen
5. beide leden delen door $n$

$$
\begin{align}
                  && I &= k \cdot i \cdot n                  &(1)\cr
\Leftrightarrow   && I &= k \cdot \frac{\frac{K}{k} - 1}{n}  &(2)\cr
\Leftrightarrow   && I &= \frac{K - k}{n}                    &(3)
\end{align}
$$

1. basisformule
2. $i$ invullen
3. teller uitwerken (distributiviteit)

## geg: $k$, $i$, $I$ - gevr: $n$, $K$

Werk zelf uit tijdens oef.

## geg: $I$, $i$, $n$ - gevr: $k$

Werk zelf uit tijdens oef.

# Afleidingen basisformule S.I.

## geg: $k$, $i$, $n$ - gevr: $K$

$$
\begin{align}
K &= k \cdot u^n \cr
  &= k \cdot (1+i)^n
\end{align}
$$

## geg: $K$, $i$, $n$ - gevr: $k$

$$
\begin{align}
                &&                  K &= k \cdot (1+i)^n    &(1)\cr
\Leftrightarrow &&  \frac{K}{(1+i)^n} &= k                  &(2)\cr
\Leftrightarrow &&                  k &= \frac{K}{(1+i)^n}  &(3)\cr
\end{align}
$$

1. basisformule
2. delen door $(1+i)^n$
3. leden van plaats verwisselen

## geg: $k$, $K$, $i$ - gevr: $n$

$$
\begin{align}
                &&                        K &= k \cdot (1+i)^n           &(1)\cr
\Leftrightarrow &&              \frac{K}{k} &= (1+i)^n                   &(2)\cr
\Leftrightarrow &&  \log _{1+i} \frac{K}{k} &= \log _{1+i} (1+i)^n       &(3)\cr
\Leftrightarrow &&  \log _{1+i} \frac{K}{k} &= n                         &(4)\cr
\Leftrightarrow &&                        n &= \log _{1+i} \frac{K}{k}   &(5)\cr
\Leftrightarrow &&                        n &= \frac{\log _{10} \frac{K}{k}}{\log _{10} (1 + i)}   &(6)
\end{align}
$$

1. basisformule
2. de factor met de gevraagde macht afzonderen
3. de logaritme met grondtal $(1+i)$ nemen van beide leden v.d. vergelijking
4. de logaritme v.e. macht met beiden hetzelfde grondtal, geeft de macht als resultaat
5. leden van plaats verwisselen
6. nuttig indien je geen log met willekeurig grondtal (*base*) hebt op je rekenmachine)

Deze afleiding wordt nog makkelijker als je werkt met $u$:

$$
\begin{align}
                &&                    K &= k \cdot u^n          \cr
\Leftrightarrow &&          \frac{K}{k} &= u^n                  \cr
\Leftrightarrow &&  \log _u \frac{K}{k} &= \log _u u^n          \cr
\Leftrightarrow &&  \log _u \frac{K}{k} &= n                    \cr
\Leftrightarrow &&                    n &= \log _u \frac{K}{k}
\end{align}
$$

## geg: $k$, $K$, $n$ - gevr: $i$

$$
\begin{align}
                &&                          K &= k \cdot (1+i)^n           &(1)\cr
\Leftrightarrow &&                \frac{K}{k} &= (1+i)^n                   &(2)\cr
\Leftrightarrow &&      \sqrt[n]{\frac{K}{k}} &= \sqrt[n]{(1+i)^n}         &(3)\cr
\Leftrightarrow &&      \sqrt[n]{\frac{K}{k}} &= 1 + i                     &(4)\cr
\Leftrightarrow &&  \sqrt[n]{\frac{K}{k}} - 1 &= i                         &(5)\cr
\Leftrightarrow &&                          i &= \sqrt[n]{\frac{K}{k}} - 1 \cr
\end{align}
$$

1. basisformule
2. afzonderen
3. van beide leden $\sqrt[n]{}$ nemen
4. n-de machtswortel en n-de macht neutraliseren elkaar
5. de $1$ overbrengen naar het andere lid

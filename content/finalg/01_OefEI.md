+++
date = 2015-09-17T00:00:00Z
title = "Oef. Enkelvoudige Intrest"
tags = [ "oef" ]
+++

#	Bereken de intrest die volgende beleggingen opleveren

| nr  | kapitaal      | rentevoet	| beleggingstijd |
|-----|---------------|-----------|----------------|
|	1   |	200,00 EUR    | 3 %	      | 1 jaar
|	2	  | 1 798,00 EUR  |	2,75%	    | 1 half jaar
|	3	  | 4 536,00 EUR  |	3,25%	    | 1 kwartaal
|	4	  | 158,00 EUR	  | 2%	      | 3 kwartalen
|	5	  | 1 088,00 EUR	| 2,25%     |	1 maand
|	6	  | 232,40 EUR	  | 1,75%	    | 8 maanden
|	7	  | 654,74 EUR	  | 3,75%	    | 11 maanden
|	8	  | 5 825,50 EUR	| 2,5%	    | 23 dagen
|	9	  | 1594,99 EUR	  | 1,5%	    | 236 dagen
|	10	| 10 094,22 EUR	| 0,75%	    | 324 dagen


<iframe width="450" height="350" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?cid=CE5446AAF555323B&resid=CE5446AAF555323B%21141&authkey=ACaYKiA9fT3Z77w&em=2&wdAllowInteractivity=False&AllowTyping=True&ActiveCell='Sheet1'!E2&wdHideGridlines=True&wdDownloadButton=True"></iframe>

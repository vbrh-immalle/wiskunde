+++
date = 2015-09-24T00:00:00Z
title = "Oef Samengestelde Intrest"
tags = [ "oef" ]
+++

# Opgave

1.	Je belegt een kapitaal van € 850 op S.I. tegen 4,8%. Hoeveel bedraagt de eindwaarde na twee jaar? En hoeveel bedraagt de verworven intrest?

2.	Welk kapitaal moet je uitzetten tegen 5% om na 9 jaar € 5280 te bezitten?

3.	Tegen welke rentevoet moet je een kapitaal van € 3 125 beleggen om na 10 jaar een eindwaarde van € 5 625 te bezitten?

4.	Je leent € 625 en moet na 2 jaar 4 maanden aan kapitaal en intresten samen € 716  terug-betalen. Tegen welke rentevoet leende je?

5.	Na hoeveel tijd verdubbelt een kapitaal, als het uitgezet werd tegen 4,5%?

6.	Hoe lang moet een kapitaal van € 10 000 uitstaan tegen 6,4%, om aan te 			groeien tot € 13 526 ?

7.	Tot welk bedrag groeit een kapitaal van € 20 250 aan, als het gedurende 8 jaar 	8 maanden uitstaat tegen 6,7%?

8.	Hoe groot is de intrest, opgebracht door een kapitaal van € 125 000, dat gedurende 10 jaar belegd wordt tegen 6%?

9.	Bereken het kapitaal dat uitstaat tegen 7% gedurende 10 jaar en een eindwaarde 		van € 277 545 oplevert.

10.	Tegen welke rentevoet moet je € 18 750 gedurende 13 jaar beleggen om een som van 	€ 48 600 te bezitten?

11.	De eindwaarde van een kapitaal van € 1 700 dat gedurende 4 jaar op S.I. uitstaat is € 2 600.  Bepaal de rentevoet.

12.	Hoe lang moet een kapitaal van € 6 250 uitstaan tegen 7% om aan te groeien tot € 8 720 ?

13.	Hoe lang moet een kapitaal van € 12 500 uitstaan tegen 8,5% om aan te groeien tot € 25 000?

# Oplossing

<iframe width="500" height="900" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?cid=CE5446AAF555323B&resid=CE5446AAF555323B%21147&authkey=ACZOSoDnoaBhXJg&em=2&wdAllowInteractivity=False&AllowTyping=True&Item='Oef%20SI'!A1%3AG70&wdDownloadButton=True"></iframe>
